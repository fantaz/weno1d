
add_definitions(-std=c++11)

if(CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")
    add_definitions(/W4)
    add_definitions(/MP)
elseif (CMAKE_COMPILER_IS_GNUCXX)# OR CMAKE_COMPILER_IS_GNUC)
      add_definitions(-Wall)
      add_definitions(-Wextra)
      add_definitions(-pedantic)
else (CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")
      message(STATUS "Unknown build tool, cannot set warning flags for your")
endif (CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")


add_executable(lagrange01 lagrange01.cpp utils.cpp)
set_property(TARGET lagrange01 PROPERTY CXX_STANDARD 11)

add_executable(weno01 weno01.cpp utils.cpp)
set_property(TARGET weno01 PROPERTY CXX_STANDARD 11)

add_executable(weno02 weno02.cpp utils.cpp)
set_property(TARGET weno02 PROPERTY CXX_STANDARD 11)
