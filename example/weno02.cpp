#include "weno1d.h"
#include "utils.h"
#include <iostream>
#include <fstream>

using vec = std::vector<double>;

int main()
{
    const size_t N = 32; // number of sampled points

    // x range
    const double startx = 0.2;
    const double endx = 0.8;

    const size_t max_stencil = 5;
    auto x = linspace(startx, endx, N, max_stencil);

    const double epsilon = 0.0001;
    vec y = calc_err_fun(x, epsilon);
    const double dx = (endx - startx) / (N - 1.);

    // print ALL sampled values
    print_points(std::cout, x, y);
    std::cout << "********************\n";

    // initialize weno interpolator
    weno1d::rational5 wi;
    const int order = wi.order();

    const int intervals = 20;
    const double frac = 1. / intervals;
    const double real_frac = dx * frac;

    std::vector<double> weno_x;
    std::vector<double> weno_y;

    for (auto iter = y.begin() + max_stencil - order + 1;
         iter != y.end() - max_stencil - order; ++iter)
    {
        const int arr_pos = iter - y.begin();
        for (int i = 0; i < intervals; ++i)
        {
            weno_x.push_back(x[arr_pos + order - 1] + i * real_frac);
            weno_y.push_back(wi(i * frac, iter)); // weno interpolation
        }
    }

    std::ofstream fout("out.txt");
    print_points(fout, weno_x, weno_y);
    return 0;
}
