#include "utils.h"
#include "weno1d.h"
#include <algorithm>
#include <iostream>
#include <vector>
int main()
{
    weno1d::rational5 wi;
    std::vector<double> y = { 0., 0., 0., 0., 0., 0.5, 0.5, 0.5, 0.5, 0.5 };

    // We're interested in interpolation of interval
    // [0 - 0.5] which is at position y[4]
    // Need to step back for R-1 points
    auto start_node = y.begin() + 4 - wi.order() + 1;

    // Interpolate at the distance 0.8 (meaning, 80% distance from [0 - 0.5])
    const double val{ 0.8 };
    std::cout << val << "\t" << wi(val, start_node) << std::endl;
    std::cout << "----" << std::endl;

    //-----------------------------------------------------------------------
    // Let's interpolate the same interval at distances
    // 0, 0.1, 0.2, ... , 1
    // Let's create a vector of distances
    auto distances = linspace(0, 1, 11);

    // Perform the interpolation and save the results
    auto points = wi(distances, start_node);

    // Print the interpolated points
    print_points(std::cout, distances, points);
    std::cout << "----" << std::endl;

    // Or, we could use weno1d this way, also
    points = wi({ 0.2, 0.8 }, start_node);
    // Print the interpolated points
    for (auto val : points) std::cout << val << std::endl;
    std::cout << "----" << std::endl;

    // Finally, one could use weno1d with std algorithms
    std::vector<double> results(distances.size());
    std::transform(distances.begin(), distances.end(), results.begin(),
        [&](double val) { return wi(val, start_node); });
    // Print the interpolated points
    print_points(std::cout, distances, results);
    std::cout << "----" << std::endl;

    return 0;
}
