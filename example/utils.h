#ifndef UTILS_H
#define UTILS_H
#include <vector>
#include <ostream>
//! Python numpy's linspace in c++
//! Creates num_in equally spaced points from start_in to end_in
std::vector<double> linspace(double start_in, double end_in, size_t num_in);

//! Python numpy's linspace in c++ with a twist
//! Creates num_in equally spaced points from start_in to end_in and expands
//! the array in both directions for max_stencil nodes
std::vector<double> linspace(
    double start_in, double end_in, size_t num_in, size_t max_stencil);

//! Calculate mean error function for an array of x coordinates:
//! $$\frac{1}{2}\left( \frac{erf(A(2x-1))}{erf(A)} -(2x-1)\right )$$
std::vector<double> calc_err_fun(
    const std::vector<double>& x_vec, const double& eps);

//! Prints points to ostream
void print_points(std::ostream& ostr, std::vector<double>& x, std::vector<double>& y);
#endif
