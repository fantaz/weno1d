#include "utils.h"
#include <algorithm>
#include <cmath>
//-----------------------------------------------------------------------------
std::vector<double> linspace(double start_in, double end_in, size_t num_in)
{
    const double dx = (end_in - start_in) / (num_in - 1);
    std::vector<double> linspaced(num_in);
    int iter = 0;
    std::generate(
        linspaced.begin(), linspaced.end(), [&] { return (iter++) * dx; });
    return linspaced;
}

//-----------------------------------------------------------------------------
std::vector<double> linspace(
    double start_in, double end_in, size_t num_in, size_t max_stencil)
{
    const double dx = (end_in - start_in) / (num_in - 1.);
    // initialize x coords
    std::vector<double> x(num_in + 2 * max_stencil);
    int iter = -max_stencil;
    std::generate(x.begin(), x.end(), [&] { return start_in + (iter++) * dx; });
    return x;
}

//-----------------------------------------------------------------------------
std::vector<double> calc_err_fun(
    const std::vector<double>& x_vec, const double& eps)
{
    const double A = 1 / (2 * std::sqrt(eps));

    std::vector<double> y(x_vec.size());
    std::transform(x_vec.begin(), x_vec.end(), y.begin(), [&A](double v) {
        return 0.5 * (std::erf(A * (2 * v - 1)) / std::erf(A) - (2 * v - 1));
    });
    return y;
}

//-----------------------------------------------------------------------------
void print_points(std::ostream& ostr, std::vector<double>& x, std::vector<double>& y)
{
    for (size_t i = 0; i < x.size(); ++i)
    {
        ostr << x[i] << "\t" << y[i] << std::endl;
    }
}
