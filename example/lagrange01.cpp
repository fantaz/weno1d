#include "barycentric_lagrange.h"
#include "utils.h"
#include <algorithm>
#include <iostream>

int main()
{
    using namespace barycentric_lagrange;
    using vec_l = std::vector<double>;

    // Init pt x coordinates
    vec_l pt_x = { 0, 1, 2 };

    // Generate array of x coordinates for which
    // the lagrange will be evaluated.
    // This will create N equally spaced points
    // from first to last pt_x
    const size_t N = 11;
    auto x = linspace(pt_x.front(), pt_x.back(), N);

    // initialize lagrange with point x and y coordinates
    lagrange2 l2(pt_x, { 0, 3, 2 });

    // initialize resulting array
    vec_l y(N);

    // compute lagrange interpolation for array of x coordinates
    std::transform(x.begin(), x.end(), y.begin(), l2);

    // print the result
    print_points(std::cout, x, y);

    return 0;
}
