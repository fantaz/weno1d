#ifndef WENO_H
#define WENO_H
#include "barycentric_lagrange.h"
#include <algorithm>
#include <array>
#include <cmath>
//! Basic usage:
//! #include "weno1d"
//! ....
//! weno1d::poly2 wi2p;
//! auto value = wi2p ( 0.5 , {0., 0., 0.5, 0.5} );
//! ...
namespace weno1d {
using namespace barycentric_lagrange;
enum class kind { polynomial, rational };
//! Class that performes WENO 1D interpolation using polynomial or rational
//! functions
//! for orders R = 2, 3, 4 and 5
//! This class uses lagrange polynomials and therefore depends on included
//! barycentric_lagrange.h
//! There are no other dependecies, apart from c++11 compiler
template <int R, kind K = kind::rational> class interpolator
{
private:
    //! smoothness indicators
    std::array<double, R> s;

    //! lagrange- polynomials
    std::array<lagrange<R>, R> ls;

    //! weno weight functions
    std::array<double, R> W;

    //! array of evaluated Lagrange polynomials
    std::array<double, R> lags;

    const double eps = 1.e-15;

    //! \beta exponent, default is R/2
    const double m_beta;

public:
    //! simple ctor, set \beta exponent, if needed
    interpolator(double _beta = R / 2.)
        : m_beta(_beta)
    {
    }

    //! get weno order
    int order() const { return R; }

    //! get beta
    double beta() const { return m_beta; }

    //! Performes WENO interpolation for point \var pt and array of values \var
    //! y.
    template <typename Iterator>
    double operator()(const double& pt, const Iterator& y)
    {
        calcSmoothnessIndicators(y);
        return calcValue(pt, y);
    }

    //! Performes WENO interpolation for points \var pts
    //! and array of values \var y.
    //! Returns an array of interpolated points.
    template <typename Iterator>
    std::vector<double> operator()(const std::vector<double>& pts, const Iterator& y)
    {
        calcSmoothnessIndicators(y);
        std::vector<double> results(pts.size());

        std::transform(pts.begin(), pts.end(), results.begin(),
            [&](double pt) { return calcValue(pt, y); });
        return results;
    }

private:
    // R = 2, polinomial and rational functions, respectively
    template <typename Iterator, int myR = R, kind myK = K>
    typename std::enable_if<myR == 2 && myK == kind::polynomial, double>::type
    calcValue(const double& x_out, const Iterator& y)
    {
        W[0] = s[1]
            * (x_out * (s[0] + 2 * s[1]) - 2 * (2 * s[0] + s[1]) * (x_out - 1))
            / ((s[0] + 2 * s[1]) * (2 * s[0] + s[1]));
        W[1] = s[0] * (2 * s[0] + s[1] + 3 * s[1] * x_out)
            / ((s[0] + 2 * s[1]) * (2 * s[0] + s[1]));

        lags[0] = ls[0](x_out + 1, { y[0], y[1], y[2] });
        lags[1] = ls[1](x_out, { y[1], y[2], y[3] });

        return lags[0] * W[0] + lags[1] * W[1];
    }

    template <typename Iterator, int myR = R, kind myK = K>
    typename std::enable_if<myR == 2 && myK == kind::rational, double>::type
    calcValue(const double& x_out, const Iterator& y)
    {
        W[0] = -(x_out - 2) / 3. / s[0];
        W[1] = (x_out + 1) / 3. / s[1];

        lags[0] = ls[0](x_out + 1, { y[0], y[1], y[2] });
        lags[1] = ls[1](x_out, { y[1], y[2], y[3] });

        return (lags[0] * W[0] + lags[1] * W[1]) / (W[0] + W[1]);
    }

    // R = 3, polinomial and rational functions, respectively
    template <typename Iterator, int myR = R, kind myK = K>
    typename std::enable_if<myR == 3 && myK == kind::polynomial, double>::type
    calcValue(const double& x_out, const Iterator& y)
    {
        W[0] = 3 * x_out * (-2 * x_out + 2)
                / (8 * s[0]
                      * (3 / (16 * s[2]) + 5 / (8 * s[1]) + 3 / (16 * s[0])))
            + x_out * (2 * x_out - 1)
                / (10 * s[0]
                      * (3 / (10 * s[2]) + 3 / (5 * s[1]) + 1 / (10 * s[0])))
            + 3 * (x_out - 1) * (2 * x_out - 1)
                / (10 * s[0]
                      * (1 / (10 * s[2]) + 3 / (5 * s[1]) + 3 / (10 * s[0])));

        W[1] = 5 * x_out * (-2 * x_out + 2)
                / (4 * s[1]
                      * (3 / (16 * s[2]) + 5 / (8 * s[1]) + 3 / (16 * s[0])))
            + 3 * x_out * (2 * x_out - 1)
                / (5 * s[1]
                      * (3 / (10 * s[2]) + 3 / (5 * s[1]) + 1 / (10 * s[0])))
            + 3 * (x_out - 1) * (2 * x_out - 1)
                / (5 * s[1]
                      * (1 / (10 * s[2]) + 3 / (5 * s[1]) + 3 / (10 * s[0])));

        W[2] = 3 * x_out * (-2 * x_out + 2)
                / (8 * s[2]
                      * (3 / (16 * s[2]) + 5 / (8 * s[1]) + 3 / (16 * s[0])))
            + 3 * x_out * (2 * x_out - 1)
                / (10 * s[2]
                      * (3 / (10 * s[2]) + 3 / (5 * s[1]) + 1 / (10 * s[0])))
            + (x_out - 1) * (2 * x_out - 1)
                / (10 * s[2]
                      * (1 / (10 * s[2]) + 3 / (5 * s[1]) + 3 / (10 * s[0])));

        lags[0] = ls[0](x_out + 2, { y[0], y[1], y[2], y[3] });
        lags[1] = ls[1](x_out + 1, { y[1], y[2], y[3], y[4] });
        lags[2] = ls[2](x_out, { y[2], y[3], y[4], y[5] });

        return lags[0] * W[0] + lags[1] * W[1] + lags[2] * W[2];
    }

    template <typename Iterator, int myR = R, kind myK = K>
    typename std::enable_if<myR == 3 && myK == kind::rational, double>::type
    calcValue(const double& x_out, const Iterator& y)
    {
        W[0] = (x_out - 3) * (x_out - 2) / 20 / s[0];
        W[1] = -(x_out - 3) * (x_out + 2) / 10 / s[1];
        W[2] = (x_out + 1) * (x_out + 2) / 20 / s[2];

        lags[0] = ls[0](x_out + 2, { y[0], y[1], y[2], y[3] });
        lags[1] = ls[1](x_out + 1, { y[1], y[2], y[3], y[4] });
        lags[2] = ls[2](x_out, { y[2], y[3], y[4], y[5] });

        return (lags[0] * W[0] + lags[1] * W[1] + lags[2] * W[2])
            / (W[0] + W[1] + W[2]);
    }

    // R = 4, polinomial and rational functions, respectively
    template <typename Iterator, int myR = R, kind myK = K>
    typename std::enable_if<myR == 4 && myK == kind::polynomial, double>::type
    calcValue(const double& x_out, const Iterator& y)
    {
        W[0] = 44 * x_out * (-3 * x_out + 2) * (-3 * x_out / 2 + 3. / 2.)
                / (189 * s[0] * (4 / (81 * s[3]) + 11 / (27 * s[2])
                                    + 88 / (189 * s[1]) + 44 / (567 * s[0])))
            + 2 * x_out * (-3 * x_out + 3) * (3 * x_out - 1)
                / (27 * s[0] * (44 / (567 * s[3]) + 88 / (189 * s[2])
                                   + 11 / (27 * s[1]) + 4 / (81 * s[0])))
            + x_out * (3 * x_out / 2 - 1. / 2.) * (3 * x_out - 2)
                / (35 * s[0] * (4 / (35 * s[3]) + 18 / (35 * s[2])
                                   + 12 / (35 * s[1]) + 1 / (35 * s[0])))
            - 4 * (x_out - 1) * (3 * x_out / 2 - 1) * (3 * x_out - 1)
                / (35 * s[0] * (1 / (35 * s[3]) + 12 / (35 * s[2])
                                   + 18 / (35 * s[1]) + 4 / (35 * s[0])));
        W[1] = 88 * x_out * (-3 * x_out + 2) * (-3 * x_out / 2 + 3. / 2.)
                / (63 * s[1] * (4 / (81 * s[3]) + 11 / (27 * s[2])
                                   + 88 / (189 * s[1]) + 44 / (567 * s[0])))
            + 11 * x_out * (-3 * x_out + 3) * (3 * x_out - 1)
                / (18 * s[1] * (44 / (567 * s[3]) + 88 / (189 * s[2])
                                   + 11 / (27 * s[1]) + 4 / (81 * s[0])))
            + 12 * x_out * (3 * x_out / 2 - 1. / 2.) * (3 * x_out - 2)
                / (35 * s[1] * (4 / (35 * s[3]) + 18 / (35 * s[2])
                                   + 12 / (35 * s[1]) + 1 / (35 * s[0])))
            - 18 * (x_out - 1) * (3 * x_out / 2 - 1) * (3 * x_out - 1)
                / (35 * s[1] * (1 / (35 * s[3]) + 12 / (35 * s[2])
                                   + 18 / (35 * s[1]) + 4 / (35 * s[0])));
        W[2] = 11 * x_out * (-3 * x_out + 2) * (-3 * x_out / 2 + 3. / 2.)
                / (9 * s[2] * (4 / (81 * s[3]) + 11 / (27 * s[2])
                                  + 88 / (189 * s[1]) + 44 / (567 * s[0])))
            + 44 * x_out * (-3 * x_out + 3) * (3 * x_out - 1)
                / (63 * s[2] * (44 / (567 * s[3]) + 88 / (189 * s[2])
                                   + 11 / (27 * s[1]) + 4 / (81 * s[0])))
            + 18 * x_out * (3 * x_out / 2 - 1. / 2.) * (3 * x_out - 2)
                / (35 * s[2] * (4 / (35 * s[3]) + 18 / (35 * s[2])
                                   + 12 / (35 * s[1]) + 1 / (35 * s[0])))
            - 12 * (x_out - 1) * (3 * x_out / 2 - 1) * (3 * x_out - 1)
                / (35 * s[2] * (1 / (35 * s[3]) + 12 / (35 * s[2])
                                   + 18 / (35 * s[1]) + 4 / (35 * s[0])));
        W[3] = 4 * x_out * (-3 * x_out + 2) * (-3 * x_out / 2 + 3. / 2.)
                / (27 * s[3] * (4 / (81 * s[3]) + 11 / (27 * s[2])
                                   + 88 / (189 * s[1]) + 44 / (567 * s[0])))
            + 22 * x_out * (-3 * x_out + 3) * (3 * x_out - 1)
                / (189 * s[3] * (44 / (567 * s[3]) + 88 / (189 * s[2])
                                    + 11 / (27 * s[1]) + 4 / (81 * s[0])))
            + 4 * x_out * (3 * x_out / 2 - 1. / 2.) * (3 * x_out - 2)
                / (35 * s[3] * (4 / (35 * s[3]) + 18 / (35 * s[2])
                                   + 12 / (35 * s[1]) + 1 / (35 * s[0])))
            - (x_out - 1) * (3 * x_out / 2 - 1) * (3 * x_out - 1)
                / (35 * s[3] * (1 / (35 * s[3]) + 12 / (35 * s[2])
                                   + 18 / (35 * s[1]) + 4 / (35 * s[0])));

        lags[0] = ls[0](x_out + 3, { y[0], y[1], y[2], y[3], y[4] });
        lags[1] = ls[1](x_out + 2, { y[1], y[2], y[3], y[4], y[5] });
        lags[2] = ls[2](x_out + 1, { y[2], y[3], y[4], y[5], y[6] });
        lags[3] = ls[3](x_out, { y[3], y[4], y[5], y[6], y[7] });

        return (
            lags[0] * W[0] + lags[1] * W[1] + lags[2] * W[2] + lags[3] * W[3]);
    }

    template <typename Iterator, int myR = R, kind myK = K>
    typename std::enable_if<myR == 4 && myK == kind::rational, double>::type
    calcValue(const double& x_out, const Iterator& y)
    {
        W[0] = -(x_out - 4) * (x_out - 3) * (x_out - 2) / 210 / s[0];
        W[1] = (x_out - 4) * (x_out - 3) * (x_out + 3) / 70 / s[1];
        W[2] = -(x_out - 4) * (x_out + 2) * (x_out + 3) / 70 / s[2];
        W[3] = (x_out + 1) * (x_out + 2) * (x_out + 3) / 210 / s[3];

        std::array<double, R> lags;
        lags[0] = ls[0](x_out + 3, { y[0], y[1], y[2], y[3], y[4] });
        lags[1] = ls[1](x_out + 2, { y[1], y[2], y[3], y[4], y[5] });
        lags[2] = ls[2](x_out + 1, { y[2], y[3], y[4], y[5], y[6] });
        lags[3] = ls[3](x_out, { y[3], y[4], y[5], y[6], y[7] });

        return (lags[0] * W[0] + lags[1] * W[1] + lags[2] * W[2]
                   + lags[3] * W[3])
            / (W[0] + W[1] + W[2] + W[3]);
    }
    // R = 5, polinomial and rational functions, respectively
    template <typename Iterator, int myR = R, kind myK = K>
    typename std::enable_if<myR == 5 && myK == kind::polynomial, double>::type
    calcValue(const double& x_out, const Iterator& y)
    {
        W[0] = 1045 * x_out * (-4 * x_out + 2) * (-2 * x_out + 3. / 2.)
                * (-4 * x_out / 3 + 4. / 3.)
                / (9216 * s[0]
                      * (1105 / (86016 * s[4]) + 4199 / (21504 * s[3])
                            + 20995 / (43008 * s[2]) + 17765 / (64512 * s[1])
                            + 1045 / (36864 * s[0])))
            + 5 * x_out * (-4 * x_out + 3) * (-2 * x_out + 2) * (4 * x_out - 1)
                / (128 * s[0]
                      * (5 / (256 * s[4]) + 15 / (64 * s[3]) + 63 / (128 * s[2])
                            + 15 / (64 * s[1]) + 5 / (256 * s[0])))
            + 1105 * x_out * (-4 * x_out + 4) * (2 * x_out - 1. / 2.)
                * (4 * x_out - 2)
                / (64512 * s[0]
                      * (1045 / (36864 * s[4]) + 17765 / (64512 * s[3])
                            + 20995 / (43008 * s[2]) + 4199 / (21504 * s[1])
                            + 1105 / (86016 * s[0])))
            + x_out * (4 * x_out / 3 - 1. / 3.) * (2 * x_out - 1)
                * (4 * x_out - 3)
                / (126 * s[0]
                      * (5 / (126 * s[4]) + 20 / (63 * s[3]) + 10 / (21 * s[2])
                            + 10 / (63 * s[1]) + 1 / (126 * s[0])))
            + 5 * (x_out - 1) * (4 * x_out / 3 - 1) * (2 * x_out - 1)
                * (4 * x_out - 1)
                / (126 * s[0]
                      * (1 / (126 * s[4]) + 10 / (63 * s[3]) + 10 / (21 * s[2])
                            + 20 / (63 * s[1]) + 5 / (126 * s[0])));

        W[1] = 17765 * x_out * (-4 * x_out + 2) * (-2 * x_out + 3. / 2.)
                * (-4 * x_out / 3 + 4. / 3.)
                / (16128 * s[1]
                      * (1105 / (86016 * s[4]) + 4199 / (21504 * s[3])
                            + 20995 / (43008 * s[2]) + 17765 / (64512 * s[1])
                            + 1045 / (36864 * s[0])))
            + 15 * x_out * (-4 * x_out + 3) * (-2 * x_out + 2) * (4 * x_out - 1)
                / (32 * s[1]
                      * (5 / (256 * s[4]) + 15 / (64 * s[3]) + 63 / (128 * s[2])
                            + 15 / (64 * s[1]) + 5 / (256 * s[0])))
            + 4199 * x_out * (-4 * x_out + 4) * (2 * x_out - 1. / 2.)
                * (4 * x_out - 2)
                / (16128 * s[1]
                      * (1045 / (36864 * s[4]) + 17765 / (64512 * s[3])
                            + 20995 / (43008 * s[2]) + 4199 / (21504 * s[1])
                            + 1105 / (86016 * s[0])))
            + 10 * x_out * (4 * x_out / 3 - 1. / 3.) * (2 * x_out - 1)
                * (4 * x_out - 3)
                / (63 * s[1]
                      * (5 / (126 * s[4]) + 20 / (63 * s[3]) + 10 / (21 * s[2])
                            + 10 / (63 * s[1]) + 1 / (126 * s[0])))
            + 20 * (x_out - 1) * (4 * x_out / 3 - 1) * (2 * x_out - 1)
                * (4 * x_out - 1)
                / (63 * s[1]
                      * (1 / (126 * s[4]) + 10 / (63 * s[3]) + 10 / (21 * s[2])
                            + 20 / (63 * s[1]) + 5 / (126 * s[0])));

        W[2] = 20995 * x_out * (-4 * x_out + 2) * (-2 * x_out + 3. / 2.)
                * (-4 * x_out / 3 + 4. / 3.)
                / (10752 * s[2]
                      * (1105 / (86016 * s[4]) + 4199 / (21504 * s[3])
                            + 20995 / (43008 * s[2]) + 17765 / (64512 * s[1])
                            + 1045 / (36864 * s[0])))
            + 63 * x_out * (-4 * x_out + 3) * (-2 * x_out + 2) * (4 * x_out - 1)
                / (64 * s[2]
                      * (5 / (256 * s[4]) + 15 / (64 * s[3]) + 63 / (128 * s[2])
                            + 15 / (64 * s[1]) + 5 / (256 * s[0])))
            + 20995 * x_out * (-4 * x_out + 4) * (2 * x_out - 1. / 2.)
                * (4 * x_out - 2)
                / (32256 * s[2]
                      * (1045 / (36864 * s[4]) + 17765 / (64512 * s[3])
                            + 20995 / (43008 * s[2]) + 4199 / (21504 * s[1])
                            + 1105 / (86016 * s[0])))
            + 10 * x_out * (4 * x_out / 3 - 1. / 3.) * (2 * x_out - 1)
                * (4 * x_out - 3)
                / (21 * s[2]
                      * (5 / (126 * s[4]) + 20 / (63 * s[3]) + 10 / (21 * s[2])
                            + 10 / (63 * s[1]) + 1 / (126 * s[0])))
            + 10 * (x_out - 1) * (4 * x_out / 3 - 1) * (2 * x_out - 1)
                * (4 * x_out - 1)
                / (21 * s[2]
                      * (1 / (126 * s[4]) + 10 / (63 * s[3]) + 10 / (21 * s[2])
                            + 20 / (63 * s[1]) + 5 / (126 * s[0])));

        W[3] = 4199 * x_out * (-4 * x_out + 2) * (-2 * x_out + 3. / 2.)
                * (-4 * x_out / 3 + 4. / 3.)
                / (5376 * s[3]
                      * (1105 / (86016 * s[4]) + 4199 / (21504 * s[3])
                            + 20995 / (43008 * s[2]) + 17765 / (64512 * s[1])
                            + 1045 / (36864 * s[0])))
            + 15 * x_out * (-4 * x_out + 3) * (-2 * x_out + 2) * (4 * x_out - 1)
                / (32 * s[3]
                      * (5 / (256 * s[4]) + 15 / (64 * s[3]) + 63 / (128 * s[2])
                            + 15 / (64 * s[1]) + 5 / (256 * s[0])))
            + 17765 * x_out * (-4 * x_out + 4) * (2 * x_out - 1. / 2.)
                * (4 * x_out - 2)
                / (48384 * s[3]
                      * (1045 / (36864 * s[4]) + 17765 / (64512 * s[3])
                            + 20995 / (43008 * s[2]) + 4199 / (21504 * s[1])
                            + 1105 / (86016 * s[0])))
            + 20 * x_out * (4 * x_out / 3 - 1. / 3.) * (2 * x_out - 1)
                * (4 * x_out - 3)
                / (63 * s[3]
                      * (5 / (126 * s[4]) + 20 / (63 * s[3]) + 10 / (21 * s[2])
                            + 10 / (63 * s[1]) + 1 / (126 * s[0])))
            + 10 * (x_out - 1) * (4 * x_out / 3 - 1) * (2 * x_out - 1)
                * (4 * x_out - 1)
                / (63 * s[3]
                      * (1 / (126 * s[4]) + 10 / (63 * s[3]) + 10 / (21 * s[2])
                            + 20 / (63 * s[1]) + 5 / (126 * s[0])));

        W[4] = 1105 * x_out * (-4 * x_out + 2) * (-2 * x_out + 3. / 2.)
                * (-4 * x_out / 3 + 4. / 3.)
                / (21504 * s[4]
                      * (1105 / (86016 * s[4]) + 4199 / (21504 * s[3])
                            + 20995 / (43008 * s[2]) + 17765 / (64512 * s[1])
                            + 1045 / (36864 * s[0])))
            + 5 * x_out * (-4 * x_out + 3) * (-2 * x_out + 2) * (4 * x_out - 1)
                / (128 * s[4]
                      * (5 / (256 * s[4]) + 15 / (64 * s[3]) + 63 / (128 * s[2])
                            + 15 / (64 * s[1]) + 5 / (256 * s[0])))
            + 1045 * x_out * (-4 * x_out + 4) * (2 * x_out - 1. / 2.)
                * (4 * x_out - 2)
                / (27648 * s[4]
                      * (1045 / (36864 * s[4]) + 17765 / (64512 * s[3])
                            + 20995 / (43008 * s[2]) + 4199 / (21504 * s[1])
                            + 1105 / (86016 * s[0])))
            + 5 * x_out * (4 * x_out / 3 - 1. / 3.) * (2 * x_out - 1)
                * (4 * x_out - 3)
                / (126 * s[4]
                      * (5 / (126 * s[4]) + 20 / (63 * s[3]) + 10 / (21 * s[2])
                            + 10 / (63 * s[1]) + 1 / (126 * s[0])))
            + (x_out - 1) * (4 * x_out / 3 - 1) * (2 * x_out - 1)
                * (4 * x_out - 1)
                / (126 * s[4]
                      * (1 / (126 * s[4]) + 10 / (63 * s[3]) + 10 / (21 * s[2])
                            + 20 / (63 * s[1]) + 5 / (126 * s[0])));

        lags[0] = ls[0](x_out + 4, { y[0], y[1], y[2], y[3], y[4], y[5] });
        lags[1] = ls[1](x_out + 3, { y[1], y[2], y[3], y[4], y[5], y[6] });
        lags[2] = ls[2](x_out + 2, { y[2], y[3], y[4], y[5], y[6], y[7] });
        lags[3] = ls[3](x_out + 1, { y[3], y[4], y[5], y[6], y[7], y[8] });
        lags[4] = ls[4](x_out, { y[4], y[5], y[6], y[7], y[8], y[9] });

        return (lags[0] * W[0] + lags[1] * W[1] + lags[2] * W[2]
            + lags[3] * W[3] + lags[4] * W[4]);
    }

    template <typename Iterator, int myR = R, kind myK = K>
    typename std::enable_if<myR == 5 && myK == kind::rational, double>::type
    calcValue(const double& x_out, const Iterator& y)
    {
        W[0] = (x_out - 5) * (x_out - 4) * (x_out - 3) * (x_out - 2) / 3024;
        W[1] = -(x_out - 5) * (x_out - 4) * (x_out - 3) * (x_out + 4) / 756;
        W[2] = (x_out - 5) * (x_out - 4) * (x_out + 3) * (x_out + 4) / 504;
        W[3] = -(x_out - 5) * (x_out + 2) * (x_out + 3) * (x_out + 4) / 756;
        W[4] = (x_out + 1) * (x_out + 2) * (x_out + 3) * (x_out + 4) / 3024;

        std::array<double, R> lags;
        lags[0] = ls[0](x_out + 4, { y[0], y[1], y[2], y[3], y[4], y[5] });
        lags[1] = ls[1](x_out + 3, { y[1], y[2], y[3], y[4], y[5], y[6] });
        lags[2] = ls[2](x_out + 2, { y[2], y[3], y[4], y[5], y[6], y[7] });
        lags[3] = ls[3](x_out + 1, { y[3], y[4], y[5], y[6], y[7], y[8] });
        lags[4] = ls[4](x_out, { y[4], y[5], y[6], y[7], y[8], y[9] });

        return (lags[0] * W[0] + lags[1] * W[1] + lags[2] * W[2]
                   + lags[3] * W[3] + lags[4] * W[4])
            / (W[0] + W[1] + W[2] + W[3] + W[4]);
    }

private:
    // This is where we calculate smoothness indicators

    //! Smoothness indicators for R = 2
    template <typename Iterator, int myR = R>
    typename std::enable_if<myR == 2>::type calcSmoothnessIndicators(
        const Iterator& y)
    {
        for (int i = 0; i < 2; i++)
        {
            s[i] = y[i + 2] + y[i] - 2. * y[i + 1];
        }
        for (int i = 0; i < 2; i++)
        {
            s[i] = eps + pow(std::abs(s[i]), m_beta);
        }
    }

    //! Smoothness indicators for R = 3
    template <typename Iterator, int myR = R>
    typename std::enable_if<myR == 3>::type calcSmoothnessIndicators(
        const Iterator& y)
    {
        s[0] = 4 * y[0] * y[0] / 3. - 9 * y[0] * y[1] + 10 * y[0] * y[2]
            - 11 * y[0] * y[3] / 3. + 16 * y[1] * y[1] - 37 * y[1] * y[2]
            + 14 * y[1] * y[3] + 22 * y[2] * y[2] - 17 * y[2] * y[3]
            + 10 * y[3] * y[3] / 3.;

        s[1] = y[1] * y[1] / 3. - y[1] * y[2] + y[1] * y[4] / 3. + y[2] * y[2]
            - y[2] * y[3] + y[3] * y[3] - y[3] * y[4] + y[4] * y[4] / 3.
            + (y[1] - 3 * y[2] + 3 * y[3] - y[4])
                * (y[1] - 3 * y[2] + 3 * y[3] - y[4]);

        s[2] = 10 * y[2] * y[2] / 3. - 17 * y[2] * y[3] + 14 * y[2] * y[4]
            - 11 * y[2] * y[5] / 3. + 22 * y[3] * y[3] - 37 * y[3] * y[4]
            + 10 * y[3] * y[5] + 16 * y[4] * y[4] - 9 * y[4] * y[5]
            + 4 * y[5] * y[5] / 3.;

        for (int i = 0; i < 3; i++)
        {
            s[i] = eps + pow(std::abs(s[i]), m_beta);
        }
    }
    //! Smoothness indicators for R = 4
    template <typename Iterator, int myR = R>
    typename std::enable_if<myR == 4>::type calcSmoothnessIndicators(
        const Iterator& y)
    {
        s[0] = 547 * pow(y[0], 2) / 240 - 311 * y[0] * y[1] / 15
            + 2131 * y[0] * y[2] / 60 - 406 * y[0] * y[3] / 15
            + 309 * y[0] * y[4] / 40 + 239 * pow(y[1], 2) / 5
            - 2491 * y[1] * y[2] / 15 + 1924 * y[1] * y[3] / 15
            - 556 * y[1] * y[4] / 15 + 2941 * pow(y[2], 2) / 20
            - 3481 * y[2] * y[3] / 15 + 4111 * y[2] * y[4] / 60
            + 469 * pow(y[3], 2) / 5 - 851 * y[3] * y[4] / 15
            + 2107 * pow(y[4], 2) / 240;

        s[1] = 89 * pow(y[1], 2) / 80 - 136 * y[1] * y[2] / 15
            + 811 * y[1] * y[3] / 60 - 131 * y[1] * y[4] / 15
            + 247 * y[1] * y[5] / 120 + 99 * pow(y[2], 2) / 5
            - 931 * y[2] * y[3] / 15 + 208 * y[2] * y[4] / 5
            - 151 * y[2] * y[5] / 15 + 1021 * pow(y[3], 2) / 20
            - 1081 * y[3] * y[4] / 15 + 1111 * y[3] * y[5] / 60
            + 407 * pow(y[4], 2) / 15 - 226 * y[4] * y[5] / 15
            + 547 * pow(y[5], 2) / 240;

        s[2] = 547 * pow(y[2], 2) / 240 - 226 * y[2] * y[3] / 15
            + 1111 * y[2] * y[4] / 60 - 151 * y[2] * y[5] / 15
            + 247 * y[2] * y[6] / 120 + 407 * pow(y[3], 2) / 15
            - 1081 * y[3] * y[4] / 15 + 208 * y[3] * y[5] / 5
            - 131 * y[3] * y[6] / 15 + 1021 * pow(y[4], 2) / 20
            - 931 * y[4] * y[5] / 15 + 811 * y[4] * y[6] / 60
            + 99 * pow(y[5], 2) / 5 - 136 * y[5] * y[6] / 15
            + 89 * pow(y[6], 2) / 80;

        s[3] = 2107 * pow(y[3], 2) / 240 - 851 * y[3] * y[4] / 15
            + 4111 * y[3] * y[5] / 60 - 556 * y[3] * y[6] / 15
            + 309 * y[3] * y[7] / 40 + 469 * pow(y[4], 2) / 5
            - 3481 * y[4] * y[5] / 15 + 1924 * y[4] * y[6] / 15
            - 406 * y[4] * y[7] / 15 + 2941 * pow(y[5], 2) / 20
            - 2491 * y[5] * y[6] / 15 + 2131 * y[5] * y[7] / 60
            + 239 * pow(y[6], 2) / 5 - 311 * y[6] * y[7] / 15
            + 547 * pow(y[7], 2) / 240;

        for (int i = 0; i < 4; i++)
        {
            s[i] = eps + pow(std::abs(s[i]), m_beta);
        }
    }

    //! Smoothness indicators for R = 5
    template <typename Iterator, int myR = R>
    typename std::enable_if<myR == 5>::type calcSmoothnessIndicators(
        const Iterator& y)
    {
        s[0] = 11329 * pow(y[0], 2) / 2520 - 253817 * y[0] * y[1] / 5040
            + 143341 * y[0] * y[2] / 1260 - 65287 * y[0] * y[3] / 504
            + 23396 * y[0] * y[4] / 315 - 86329 * y[0] * y[5] / 5040
            + 357061 * pow(y[1], 2) / 2520 - 1621843 * y[1] * y[2] / 2520
            + 928931 * y[1] * y[3] / 1260 - 2144281 * y[1] * y[4] / 5040
            + 62227 * y[1] * y[5] / 630 + 231797 * pow(y[2], 2) / 315
            - 2141929 * y[2] * y[3] / 1260 + 1247711 * y[2] * y[4] / 1260
            - 117031 * y[2] * y[5] / 504 + 312752 * pow(y[3], 2) / 315
            - 2955763 * y[3] * y[4] / 2520 + 352081 * y[3] * y[5] / 1260
            + 888991 * pow(y[4], 2) / 2520 - 865337 * y[4] * y[5] / 5040
            + 53959 * pow(y[5], 2) / 2520;
        s[1] = 1727 * pow(y[1], 2) / 1260 - 74687 * y[1] * y[2] / 5040
            + 40021 * y[1] * y[3] / 1260 - 16945 * y[1] * y[4] / 504
            + 22079 * y[1] * y[5] / 1260 - 18079 * y[1] * y[6] / 5040
            + 103171 * pow(y[2], 2) / 2520 - 451093 * y[2] * y[3] / 2520
            + 242861 * y[2] * y[4] / 1260 - 513631 * y[2] * y[5] / 5040
            + 13297 * y[2] * y[6] / 630 + 126229 * pow(y[3], 2) / 630
            - 556639 * y[3] * y[4] / 1260 + 300611 * y[3] * y[5] / 1260
            - 25345 * y[3] * y[6] / 504 + 78812 * pow(y[4], 2) / 315
            - 701413 * y[4] * y[5] / 2520 + 76351 * y[4] * y[6] / 1260
            + 25352 * pow(y[5], 2) / 315 - 185567 * y[5] * y[6] / 5040
            + 11329 * pow(y[6], 2) / 2520;
        s[2] = 1727 * pow(y[2], 2) / 1260 - 64817 * y[2] * y[3] / 5040
            + 29731 * y[2] * y[4] / 1260 - 10687 * y[2] * y[5] / 504
            + 11789 * y[2] * y[6] / 1260 - 8209 * y[2] * y[7] / 5040
            + 20359 * pow(y[3], 2) / 630 - 313963 * y[3] * y[4] / 2520
            + 146261 * y[3] * y[5] / 1260 - 265201 * y[3] * y[6] / 5040
            + 11789 * y[3] * y[7] / 1260 + 79399 * pow(y[4], 2) / 630
            - 309889 * y[4] * y[5] / 1260 + 146261 * y[4] * y[6] / 1260
            - 10687 * y[4] * y[7] / 504 + 79399 * pow(y[5], 2) / 630
            - 313963 * y[5] * y[6] / 2520 + 29731 * y[5] * y[7] / 1260
            + 20359 * pow(y[6], 2) / 630 - 64817 * y[6] * y[7] / 5040
            + 1727 * pow(y[7], 2) / 1260;
        s[3] = 11329 * pow(y[3], 2) / 2520 - 185567 * y[3] * y[4] / 5040
            + 76351 * y[3] * y[5] / 1260 - 25345 * y[3] * y[6] / 504
            + 13297 * y[3] * y[7] / 630 - 18079 * y[3] * y[8] / 5040
            + 25352 * pow(y[4], 2) / 315 - 701413 * y[4] * y[5] / 2520
            + 300611 * y[4] * y[6] / 1260 - 513631 * y[4] * y[7] / 5040
            + 22079 * y[4] * y[8] / 1260 + 78812 * pow(y[5], 2) / 315
            - 556639 * y[5] * y[6] / 1260 + 242861 * y[5] * y[7] / 1260
            - 16945 * y[5] * y[8] / 504 + 126229 * pow(y[6], 2) / 630
            - 451093 * y[6] * y[7] / 2520 + 40021 * y[6] * y[8] / 1260
            + 103171 * pow(y[7], 2) / 2520 - 74687 * y[7] * y[8] / 5040
            + 1727 * pow(y[8], 2) / 1260;
        s[4] = 53959 * pow(y[4], 2) / 2520 - 865337 * y[4] * y[5] / 5040
            + 352081 * y[4] * y[6] / 1260 - 117031 * y[4] * y[7] / 504
            + 62227 * y[4] * y[8] / 630 - 86329 * y[4] * y[9] / 5040
            + 888991 * pow(y[5], 2) / 2520 - 2955763 * y[5] * y[6] / 2520
            + 1247711 * y[5] * y[7] / 1260 - 2144281 * y[5] * y[8] / 5040
            + 23396 * y[5] * y[9] / 315 + 312752 * pow(y[6], 2) / 315
            - 2141929 * y[6] * y[7] / 1260 + 928931 * y[6] * y[8] / 1260
            - 65287 * y[6] * y[9] / 504 + 231797 * pow(y[7], 2) / 315
            - 1621843 * y[7] * y[8] / 2520 + 143341 * y[7] * y[9] / 1260
            + 357061 * pow(y[8], 2) / 2520 - 253817 * y[8] * y[9] / 5040
            + 11329 * pow(y[9], 2) / 2520;

        for (int i = 0; i < 5; i++)
        {
            s[i] = eps + pow(std::abs(s[i]), m_beta);
        }
    }
};

// Utility types
using poly2 = interpolator<2, weno1d::kind::polynomial>;
using poly3 = interpolator<3, weno1d::kind::polynomial>;
using poly4 = interpolator<4, weno1d::kind::polynomial>;
using poly5 = interpolator<5, weno1d::kind::polynomial>;

using rational2 = interpolator<2, weno1d::kind::rational>;
using rational3 = interpolator<3, weno1d::kind::rational>;
using rational4 = interpolator<4, weno1d::kind::rational>;
using rational5 = interpolator<5, weno1d::kind::rational>;
}
#endif // WENO_H
