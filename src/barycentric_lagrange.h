#ifndef BARYCENTRIC_LAGRANGE_H
#define BARYCENTRIC_LAGRANGE_H
#include <algorithm>
#include <cmath>
#include <limits>
#include <numeric>
#include <type_traits>
#include <vector>
namespace barycentric_lagrange
{

////! shamefully copied from cppreference.com
template <class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
almost_equal(T x, T y, int ulp = 4)
{
    // the machine epsilon has to be scaled to the magnitude of the values used
    // and multiplied by the desired precision in ULPs (units in the last place)
    return std::abs(x - y)
        < std::numeric_limits<T>::epsilon() * std::abs(x + y) * ulp
        // unless the result is subnormal
        || std::abs(x - y) < std::numeric_limits<T>::min();
}

using std::vector;

//! Lagrange interpolation using barycentric formulation
//! N is a Lagrange's polynom order
template <int N> class lagrange
{
private:
    vector<double> x;
    vector<double> y; // N + 1
    vector<double> w;

public:
    //! default nodes in this ctor
    //! x is [0, 1, ..., N]
    //! y is [0, 0, ..., 0]
    //! w is [1, 1, ..., 1]
    lagrange()
        : x(N + 1)
        , y(N + 1, 0)
        , w(N + 1, 1)
    {
        std::iota(x.begin(), x.end(), 0);
        init();
    }

    //! x is set outside
    //! y is zero \forall elements in an array
    //! w is one  \forall elements in an array
    //! Calls lagrange(_x, _y)
    lagrange(vector<double> _x)
        : lagrange(_x, std::vector<double>(N + 1, 0))
    {
    }

    //! x and y are set outside
    //! w is one \forall elements in an array
    lagrange(vector<double> _x, vector<double> _y)
        : x(std::move(_x))
        , y(std::move(_y))
        , w(N + 1, 1)
    {
        init();
    }

    //! Calculates interpolating value for point \pt and array of y values \_y
    double operator()(const double& pt, const std::vector<double>& _y) const
    {
        double sumb{ 0. }, p{ 0. }, b{ 1. };
        for (int i = 0; i < N + 1; ++i)
        {
            if (almost_equal(pt, x[i]))
                return _y[i]; // a hit directly in the node

            b = 1. / (pt - x[i]) / w[i];
            sumb += b;
            p += b * _y[i];
        }
        return p / sumb;
    }

    //! Calculates interpolating value for point \pt
    //! In this case, the y array was already set in constructor
    double operator()(const double& pt) const { return (*this)(pt, y); }

private:
    //! caclulates ws
    void init()
    {
        // I'm pretty sure that this could be written more efficiently
        for (int i = 0; i < N + 1; ++i)
        {
            for (int j = 0; j < N + 1; ++j)
            {
                if (i != j)
                {
                    w[i] *= (x[i] - x[j]);
                }
            }
        }
    }
};

// utility typedefs
using lagrange2 = lagrange<2>;
using lagrange3 = lagrange<3>;
using lagrange4 = lagrange<4>;
using lagrange5 = lagrange<5>;
}
#endif
