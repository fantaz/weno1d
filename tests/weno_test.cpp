#include "weno1d.h"
#include <cmath>
#include <iostream>

int evaluator(const double& result, const double& expected)
{
    if( std::abs(result - expected) > 0.00001) 
    { 
        std::cout<<"eval is "<<result<< " but should be "<< expected<<" !"<<std::endl;
        return 1;
    }
    return 0;
}
int weno_test(int , char **)
{
    int success = 0;
    
    // WENO 2
    std::array<double, 4> y2 = {0.,0. ,0.5, 0.5};
    weno1d::interpolator<2,weno1d::kind::polynomial> wi2p;
    success += evaluator(wi2p ( 0. , y2 ), 0.  );
    success += evaluator(wi2p ( 0.5, y2 ), 0.25);
    success += evaluator(wi2p ( 1. , y2 ), 0.5 );

    weno1d::interpolator<2,weno1d::kind::rational> wi2r;
    success += evaluator(wi2r ( 0. , y2 ), 0.  );
    success += evaluator(wi2r ( 0.5, y2 ), 0.25);
    success += evaluator(wi2r ( 1. , y2 ), 0.5 );
    
    // WENO 3
    std::array<double, 6> y3 = {0.,0.,0.0,0.5,0.5,0.5};
    weno1d::interpolator<3,weno1d::kind::polynomial> wi3p;
    success += evaluator(wi3p ( 0. , y3 ),  0.);
    success += evaluator(wi3p ( 0.5, y3 ),  0.25);
    success += evaluator(wi3p ( 1. , y3 ),  0.5);

    weno1d::interpolator<3,weno1d::kind::rational> wi3r;
    success += evaluator(wi3r ( 0. , y3 ),  0.  );
    success += evaluator(wi3r ( 0.5, y3 ),  0.25);
    success += evaluator(wi3r ( 1. , y3 ),  0.5 );

    // WENO 4
    std::array<double, 8> y4 = {0., 0., 0., 0., 0.5, 0.5, 0.5, 0.5};
    weno1d::interpolator<4,weno1d::kind::polynomial> wi4p;
    success += evaluator(wi4p ( 0.  , y4 ), 0.       );
    success += evaluator(wi4p ( 0.5 , y4 ), 0.25     );
    success += evaluator(wi4p ( 1.  , y4 ), 0.5      );
    
    weno1d::interpolator<4,weno1d::kind::rational> wi4r;
    success += evaluator(wi4r ( 0.5 , y4 ), 0.25 );
    
    // WENO 5
    std::array<double, 10> y5 = {0.,0.,0.,0.,0.0,0.5,0.5,0.5,0.5,0.5};
    weno1d::interpolator<5,weno1d::kind::polynomial> wi5p;
    success += evaluator(wi5p ( 0.  , y5 ), 0.       );
    success += evaluator(wi5p ( 0.5 , y5 ), 0.25     );
    success += evaluator(wi5p ( 1 ,   y5 ), 0.5 );

    weno1d::interpolator<5,weno1d::kind::rational> wi5r;
    success += evaluator(wi5r ( 0.  , y5 ), 0.       );
    success += evaluator(wi5r ( 0.5 , y5 ), 0.25     );
    success += evaluator(wi5r ( 1 ,   y5 ), 0.5 );
    
    // Testing rational weno on
    // array of equal values
    double num = 10.;
    {
        std::vector<double> v(4);
        std::generate(v.begin(), v.end(), [=](){return num;});
        weno1d::rational2 wi;
        auto start_node = v.begin() + 1 - wi.order() + 1;
        success += evaluator(wi ( 0.8 ,   start_node ), num );
    }

    {
        std::vector<double> v(6);
        std::generate(v.begin(), v.end(), [=](){return num;});
        weno1d::rational3 wi;
        auto start_node = v.begin() + 2 - wi.order() + 1;
        success += evaluator(wi ( 0.8 ,   start_node ), num );
    }

    {
        std::vector<double> v(8);
        std::generate(v.begin(), v.end(), [=](){return num;});
        weno1d::rational4 wi;
        auto start_node = v.begin() + 3 - wi.order() + 1;
        success += evaluator(wi ( 0.8 ,   start_node ), num );
    }
    {
        std::vector<double> v(10);
        std::generate(v.begin(), v.end(), [=](){return num;});
        weno1d::rational5 wi;
        auto start_node = v.begin() + 4 - wi.order() + 1;
        success += evaluator(wi ( 0.8 ,   start_node ), num );
    }
    return success;
}
