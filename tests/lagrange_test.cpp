#include "barycentric_lagrange.h"
#include <array>
#include <cmath>
#include <iostream>

/*
 * DISCLAIMER
 * Calculation results are compared with values
 * obtained by the Python's SciPy.interpolate.lagrange function
 * for the same input results
 */
int lagrange_test(int, char**)
{

    using namespace barycentric_lagrange;
    int success = 0;
    double eval;
    const double eps = 1.e-06;

    std::vector<double> y2{ 0, 3, 2 };
    // uniform N=2
    lagrange2 l2;
    eval = l2(0.5, y2);
    if (std::abs(eval - 2) > eps)
    {
        std::cout << "eval is " << eval << " but the result should be 2!"
                  << std::endl;
        success++;
    }

    // non uniform N=2
    lagrange2 ln2({ 0, 0.8, 1 });
    eval = ln2(0.5, y2);
    if (std::abs(eval - 3.1875) > eps)
    {
        std::cout << "eval is " << eval << " but the resuls should be 3.1875!"
                  << std::endl;
        success++;
    }

    std::vector<double> y3{ 0, 3, 2, 1 };
    // uniform N=3
    lagrange3 l3;
    eval = l3(2.5, y3);
    if (std::abs(eval - 1.25) > eps)
    {
        std::cout << "eval is " << eval << " but should be 1.25!" << std::endl;
        success++;
    }

    // non uniform N=3
    lagrange3 ln3({ 0, 0.2, 0.6, 1 });
    eval = ln3(0.5, y3);
    if (std::abs(eval - 2.6875) > eps)
    {
        std::cout << "eval is " << eval << " but should be 2.6875!"
                  << std::endl;
        success++;
    }

    std::vector<double> y4{ 0, 3, 2, 1, 3 };
    // uniform N=4
    lagrange4 l4;
    eval = l4(0.5, y4);
    if (std::abs(eval - 2.2890625) > eps)
    {
        std::cout << "eval is " << eval << " but should be 2.2890625!"
                  << std::endl;
        success++;
    }

    // non uniform N=4
    lagrange4 ln4({ 0, 0.2, 0.5, 0.75, 1 });
    eval = ln4(0.5, y4);
    if (std::abs(eval - 2.) > eps)
    {
        std::cout << "eval is " << eval << " but should be 2.!" << std::endl;
        success++;
    }

    return success;
}
