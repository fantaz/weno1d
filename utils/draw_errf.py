
import csv
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf

if __name__ == '__main__':

    x_range = [0.2, 0.8]
    sampled_pts = 32
    eps = 0.0001

    # read weno output
    with open('../build/example/out.txt', 'r') as dest_f:
        data_iter = csv.reader(dest_f,
                               delimiter='\t',
                               quotechar='"')
        data = [data for data in data_iter]

    data_array = np.asarray(data, dtype=float)

    # draw real function on 512 pts
    X = np.linspace(x_range[0], x_range[1], 512, endpoint=True)

    A = 1./(2*np.sqrt(eps))
    Y = 0.5*(erf(A*(2*X-1))/erf(A) - (2*X-1))

    # draw sampled points
    ptx = np.linspace(x_range[0], x_range[1], sampled_pts, endpoint=True)
    pty = 0.5 * (erf(A * (2 * ptx - 1)) / erf(A) - (2 * ptx - 1))

    # draw calculated weno
    plt.plot(data_array[:, 0], data_array[:, 1], 'ro')
    plt.plot(X, Y, 'b')
    plt.plot(ptx, pty, 'bo')

    plt.show()


