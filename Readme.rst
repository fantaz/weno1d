********
WENO 1d
********
A small module that performes 1D WENO interpolation with polynomial or rational functions for r = 2, 3, 4 and 5. 
The approximation is :math:`\mathcal{O}(2r)` accurate. 

It is a header only library and does not have any dependencies, apart from C++11 compiler.

Additionally, since our implementation uses barycentric Lagrange interpolation, one can use Lagrange interpolation as 
a standalone module.

************
Basic Usage
************

====================
Barycentric Lagrange
====================

We'd like to calculate Lagrange interpolation for x=1 for node values 
(0,0), (1,3), (2,2)

.. code-block:: c++

  #include "barycentric_lagrange.h"
  int main()
  {
    using namespace barycentric_lagrange;
    lagrange2 l22({0,1,2}, {0,3,2});
    auto val = l22(1); // val should be equal to 3
    return 0;
  }
  

------------------------------------
A slightly more complicated example
------------------------------------
Given the above node values, we'd like to compute values for points x={0,0.2, 0.4, ..., 2}.

.. code-block:: c++

  #include "barycentric_lagrange.h"
  #include "utils.h"
  #include <algorithm>
  #include <iostream>

  int main()
  {
     using namespace barycentric_lagrange;
     using vec_l = std::vector<double>;

     // Init pt x coordinates
     vec_l pt_x = { 0, 1, 2 };

     // Generate array of x coordinates for which
     // the lagrange will be evaluated.
     // This will create N equally spaced points
     // from first to last pt_x
     const size_t N = 11;
     auto x = linspace(pt_x.front(), pt_x.back(), N);

     // initialize lagrange with point x and y coordinates
     lagrange2 l2(pt_x, { 0, 3, 2 });

     // initialize resulting array
     vec_l y(N);

     // compute lagrange interpolation for array of x coordinates
     std::transform(x.begin(), x.end(), y.begin(), l2);

     // print the result
     print_points(std::cout, x, y);

     return 0;
  }



=========
WENO 1D
=========
    
We'd like to calculate WENO interpolation for x = 0.5 for node values
(-1, 0),(0, 0), (1, 0.5),  (2, 0.5).

.. code-block:: c++
   
  #include "weno1d.h"
  int main()
  {
    weno1d::poly2 wi2p;
    auto val = wi2p( 0.5, {0.,0. ,0.5, 0.5});
    return 0;
  }
  
One could also perform WENO interpolation for an array of x values:

.. code-block:: c++
   
  #include "weno1d.h"
  int main()
  {
    weno1d::poly2 wi2p;
    auto vals = wi2p( {0.2, 0.4, 0.6}, {0.,0. ,0.5, 0.5});
    return 0;
  }

The weno1d object now returns an array of interpolated values. Furthermore, one can easily use weno1d with std algorithms.

**********************
Lagrange interpolation
**********************

What we compute in Lagrange interpolation:

Given the interpolating points :math:`\{x_0,...,x_n\}` and the function :math:`f(x)`,
the interpolating polynomial is defined as

.. math::
   p(x) = \omega(x)\sum\limits_{j=0}^n \frac{f(x_j)}{\omega'(x_j)(x-x_j)},

where :math:`\omega(x)=(x-x_0)(x-x_1)\cdots(x-x_n)` and barycentric weights are calculated as 

.. math::
   \omega'(x_j) = \left(\prod_{k=0,j\neq k}^{n} (x_j-x_k)\right)

If we set 

.. math::
   b_j(x) = \frac{1}{\omega'(x_j)(x-x_j)}

Barycentric Lagrange interpolating polynomial [1] can be written in the form:

.. math::
   p(x) = \frac{\sum\limits_{j=0}^n b_j(x)\,f(x_j)}{\sum\limits_{j=0}^n b_j(x)}.

******************
WENO interpolation
******************

WENO interpolation is a rational weighted function:
  
.. math::
   w(x) = \frac{\sum\limits_{j=0}^n w_j(x)\,p_j(x)}{\sum\limits_{j=0}^n w_j(x)},
where the polynomial :math:`p_j(x)` is a Lagrange polynomial on the stencil :math:`s_j=\{x_{k+i-r+1},\| k=j,...,j+r\}`. The rational weights :math:`w_j(x)=\frac{1}{\epsilon +SI_j^d}` reduce oscillations in :math:`w(x)` using smoothness indicators 

.. math::
   SI_j=\int_{x_i}^{x_{i+1} }p_j'(x)^2 \operatorname{dx}.





[1] J.-P. Berrut, L.N. Trefethen: Barycentric Lagrange interpolation, SIAM Review 46 (2004)

************
Licence
************
The MIT License (MIT)

Copyright (c) 2015 Jerko Škifić, Bojan Crnković

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. This README would normally document whatever steps are necessary to get your application up and running.
